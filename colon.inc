%define last_pair 0
%macro colon 2
    %2:
    dq last_pair
    db %1, 0
    %define last_pair %2
%endmacro
