global find_word
extern string_equals

section .text

; в rdi адрес ключа
; в rsi адрес последнего элемента в словаре
; возвращает адрес элемента в rax

find_word:
    .loop:
		test rsi, rsi
		jz .end
		push rsi  
		add rsi, 8
		push rdi 
		call string_equals
		pop rdi
		pop rsi
		test rax, rax
		jnz .found
		
		mov rsi, [rsi] ; следующий элемент
		jmp .loop
    .found:
		mov rax, rsi
    .end:
		ret
