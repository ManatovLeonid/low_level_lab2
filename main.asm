extern print_newline
extern string_length
extern find_word
extern read_word
extern print_string
extern exit
global _start

section .data

enter_key_text: db "Enter key: ", 0
here_value_text: db "Value: ", 0
no_key_error_text: db "No key found", 0

section .text

%include "colon.inc"
%include "words.inc"

_start:
	push rbp
	mov rbp, rsp 
	sub rsp , 256
	
	mov rdi, enter_key_text
	call print_string
	
	mov rdi, rsp
	mov rsi, 256
	
    call read_word
	
	mov rdi, rax
    mov rsi, last_pair
    call find_word
    test rax, rax
    jz .no_key_error_

	push rax 
	mov rdi, here_value_text
	call print_string
	pop rax


    add rax, 8
	push rax ;сохраняем адрес ключа
    mov rdi, rax
	call string_length
	pop rdi ;восстанавливаем адрес ключа
    add rdi, rax
    inc rdi
    call print_string
    call print_newline
	jmp .end

	.no_key_error_:
		mov rdi, no_key_error_text
		call print_string
		call print_newline
		
	.end:
		mov rsp, rbp 
		pop rbp 
		xor rdi,rdi
		call exit
